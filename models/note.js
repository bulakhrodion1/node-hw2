const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const noteSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        required: true
    },
    completed: {
        type: Boolean,
        required: false,
        default: false
    },
    text: {
        type: String,
        required: true,
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }

});

module.exports = mongoose.model('Note', noteSchema);