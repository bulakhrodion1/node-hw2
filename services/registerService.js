const bcrypt = require('bcryptjs');
const User = require('../models/user');

const registerUser = async (username, password) => {
    const hashedPassword = await bcrypt.hash(password, 12);
    const user = new User({
        username,
        password: hashedPassword
    });
    await user.save();
}

module.exports = registerUser;
