const Mongoose = require("mongoose");
const ObjectId = Mongoose.Types.ObjectId;

const Note = require("../models/note");

const getNotesByUserId = async (id, offset = 0, limit = 0) => {
    try {
        const count = await Note.count({ userId: id });
        const notes = await Note.find({ userId: id }, "-__v")
            .skip(+offset)
            .limit(+limit);
        return {
            offset: offset,
            limit: limit,
            count,
            notes,
        };
    } catch (error) {
        console.log(error);
        const er = new Error("Failed to get notes");
        er.status = 400;
        throw er;
    }
};

const createUserNote = async (text, userId) => {
    try {
        const note = new Note({
            userId: new ObjectId(userId),
            text,
        });
        await note.save();
    } catch (error) {
        console.log(error);
        const er = new Error("Failed to create note");
        er.status = 400;
        throw er;
    }
};

const getNoteById = async (noteId, userId) => {
    return Note.findOne({_id: noteId, userId}, "-__v");
};

const updateNoteTextById = async (noteId, userId, text) => {
    const note = await Note.findOne({ _id: noteId, userId });
    if (!note) {
        const er = new Error("Note not found");
        er.status = 400;
        throw er;
    }
    note.text = text;
    return await note.save();
};

const toggleNoteCompleteStatus = async (noteId, userId) => {
    const note = await Note.findOne({ _id: noteId, userId });
    if (!note) {
        const er = new Error("Note not found");
        er.status = 400;
        throw er;
    }
    note.completed = !note.completed;
    return await note.save();
};

const deleteNoteById = async (noteId, userId) => {
    let result = await Note.deleteOne({ _id: noteId, userId });
    if(result.deletedCount === 0) {
        const er = new Error("Note not found");
        er.status = 400;
        throw er;
    }
}

module.exports = {
    createUserNote,
    getNotesByUserId,
    getNoteById,
    updateNoteTextById,
    toggleNoteCompleteStatus,
    deleteNoteById
};
