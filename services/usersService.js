const User = require('../models/user');
const getUserById = async (id) => {
    return User.findOne({_id: id});
};
const deleteUserById = async (id) => {
    const result = await User.deleteOne({_id: id})
    if(result.deletedCount === 0) {
        const er = new Error("User not found");
        er.status = 400;
        throw er;
    }
}

module.exports = {
    getUserById,
    deleteUserById
};
