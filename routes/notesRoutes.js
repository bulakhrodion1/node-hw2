const express = require('express');
const authMiddleware = require('../midlewares/authMiddleware');
const controllers = require('../controllers/notesControllers');

const router = express.Router();

router.get('/', authMiddleware, controllers.getNotes);
router.post('/', authMiddleware, controllers.createNote);
router.get('/:id', authMiddleware, controllers.getNote);
router.put('/:id', authMiddleware, controllers.updateNodeText);
router.patch('/:id', authMiddleware, controllers.toggleNodeStatus);
router.delete('/:id', authMiddleware, controllers.deleteNote);


module.exports = router;