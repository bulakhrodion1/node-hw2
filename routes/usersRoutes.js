const express = require('express');
const authMiddleware = require('../midlewares/authMiddleware');
const controllers = require('../controllers/usersControllers')

const router = express.Router();

router.get('/me', authMiddleware, controllers.getUser);
router.patch('/me', authMiddleware, controllers.updateUserPassword);
router.delete('/me', authMiddleware, controllers.deleteUser);

module.exports = router;
