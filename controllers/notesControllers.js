const {
    createUserNote,
    getNotesByUserId,
    getNoteById,
    updateNoteTextById,
    toggleNoteCompleteStatus,
    deleteNoteById
} = require('../services/notesService');

const getNotes = async (req, res) => {
    let notes;
    try {
        const {offset, limit} = req.query;
        notes =  await getNotesByUserId(req.user.id, offset, limit)
    } catch (error) {
        if(error.status && error.message) {
            res.status(error.status).json({
                message: error.message
            });
            return;
        }
        console.log(error);
        res.status(500).json({
            message: "Internal server error",
        });
        return;
    }

    res.status(200).json(notes);
};

const createNote = async (req, res) => {
    try {
        const { text } = req.body;
        await createUserNote(text, req.user.id)
    } catch (error) {
        if(error.status && error.message) {
            res.status(error.status).json({
                message: error.message
            });
            return;
        }
        console.log(error);
        res.status(500).json({
            message: "Internal server error",
        });
        return;
    }
    res.status(200).json({
        message: "Success"
    });
};

const getNote = async (req, res) => {
    const noteId = req.params.id;
    try {
        const note = await getNoteById(noteId, req.user.id);
        if(!note) {
            res.status(400).json({
                message: "Note not found",
            });
            return;
        }
        res.status(200).json({
            note
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: "Internal server error",
        });
    }
};

const updateNodeText = async (req, res) => {
    const noteId = req.params.id;
    const text = req.body.text;
    if (text === undefined) {
        res.status(400).json({
            message: "text is not specified",
        });
        return;
    }
    try {
        await updateNoteTextById(noteId, req.user.id, text);
        res.status(200).json({
            message: "Success"
        });
    } catch (error) {
        if(error.status && error.message) {
            res.status(error.status).json({
                message: error.message
            });
            return;
        }
        console.log(error);
        res.status(500).json({
            message: "Internal server error",
        });
    }
};

const toggleNodeStatus = async (req, res) => {
    const noteId = req.params.id;
    try {
        await toggleNoteCompleteStatus(noteId, req.user.id);
        res.status(200).json({
            message: "Success"
        });
    } catch (error) {
        if(error.status && error.message) {
            res.status(error.status).json({
                message: error.message
            });
            return;
        }
        console.log(error);
        res.status(500).json({
            message: "Internal server error",
        });
    }
};

const deleteNote = async (req, res) => {
    const noteId = req.params.id;
    try {
        await deleteNoteById(noteId, req.user.id);
        res.status(200).json({
            message: "Success"
        });
    } catch (error) {
        if(error.status && error.message) {
            res.status(error.status).json({
                message: error.message
            });
            return;
        }
        console.log(error);
        res.status(500).json({
            message: "Internal server error",
        });
    }
}

module.exports = {
    getNotes,
    createNote,
    getNote,
    updateNodeText,
    toggleNodeStatus,
    deleteNote
}