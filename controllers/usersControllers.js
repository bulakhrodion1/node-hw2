const bcrypt = require('bcryptjs');

const User = require('../models/user');

const {
    getUserById,
    deleteUserById
} = require('../services/usersService')

const getUser = async (req, res) => {
    let user;
    try {
        user = await getUserById(req.user.id)
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: "Internal server error",
        });
        return;
    }

    res.status(200).json({
        user: {
            _id: user._id,
            username: user.username,
            createdDate: user.createdDate
        }
    });
};

const updateUserPassword = async (req, res) => {
    try {
        const {oldPassword, newPassword} = req.body;
        if (!oldPassword || !newPassword) {
            res.status(400).json({
                message: "oldPassword or newPassword is not specified",
            });
            return;
        }
        const user = await User.findOne({ username: req.user.username });
        if (!user) {
            res.status(400).json({
                message: "User not found",
            });
            return;
        }
        const isEqual = await bcrypt.compare(oldPassword, user.password);
        if (!isEqual) {
            res.status(400).json({
                message: "Invalid password",
            });
            return;
        }
        user.password = await bcrypt.hash(newPassword, 12);
        await user.save();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: "Internal server error",
        });
        return;
    }

    res.status(200).json({
        message: "Success",
    });
};

const deleteUser = async (req, res) => {
    try {
        await deleteUserById(req.user.id);
        res.status(200).json({
            message: "Success",
        });
    } catch (error) {
        if(error.status && error.message) {
            res.status(error.status).json({
                message: error.message
            });
            return;
        }
        console.log(error);
        res.status(500).json({
            message: "Internal server error",
        });
    }
}

module.exports = {
    getUser,
    updateUserPassword,
    deleteUser
}
