const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const registerNewUser = require("../services/registerService");
const User = require("../models/user");

const SECRET = process.env.SECRET;

const register = async (req, res) => {
    const { username, password } = req.body;
    if (!username || !password) {
        res.status(400).json({
            message: "username or password is not specified",
        });
        return;
    }
    try {
        await registerNewUser(username, password);
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: "Username is already taken",
        });
        return;
    }

    res.status(200).json({
        message: "Success",
    });

};

const login = async (req, res) => {
    const { username, password } = req.body;
    if (!username || !password) {
        res.status(400).json({
            message: "username or password is not specified",
        });
        return;
    }
    let token;
    try {
        const user = await User.findOne({ username });
        if (!user) {
            res.status(400).json({
                message: "Invalid username",
            });
            return;
        }
        const isEqual = await bcrypt.compare(password, user.password);
        if (!isEqual) {
            res.status(400).json({
                message: "Invalid password",
            });
            return;
        }
        token = jwt.sign(
            {
                id: user._id,
                username: user.username,
            },
            SECRET,
            {
                expiresIn: 80000,
            }
        );
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: "Internal server error",
        });
        return;
    }

    res.status(200).json({
        message: "Success",
        jwt_token: token
    });
};
module.exports = {
    register,
    login,
};