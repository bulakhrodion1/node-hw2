const jwt = require("jsonwebtoken");
const User = require("../models/user");

const SECRET = process.env.SECRET;

const authMiddleware = async (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        if(!token){
            return res.status(400).json({
                message: "No token was passed",
            })
        }

        let userInfo = jwt.verify(token, SECRET);

        const userExists = await User.exists({_id: userInfo.id});
        if (!userExists) {
            return res.status(400).json({
                message: "User is not found",
            });
        }

        req.user = {
            id: userInfo.id,
            username: userInfo.username
        }
        next();
    } catch (error) {
        console.log(error);
        res.status(400).json({
            message: "User is not authorized",
        })
    }
};

module.exports = authMiddleware;
