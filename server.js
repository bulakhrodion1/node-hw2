require("dotenv").config();

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const authRoutes = require("./routes/authRoutes");
const usersRoutes = require("./routes/usersRoutes");
const notesRoutes = require("./routes/notesRoutes");

const PORT = +process.env.PORT || 8080;
const DB_CONNECTION = process.env.DB_CONNECTION;

const app = express();

app.use(express.json());
app.use(cors());

app.use('/api/auth', authRoutes);
app.use('/api/users', usersRoutes);
app.use('/api/notes', notesRoutes);

const initialize = async () => {
    try {
        await mongoose.connect(DB_CONNECTION, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });

        app.listen(PORT || 8080);
    } catch (err) {
        console.error(`Failed to start server: ${err.message}`);
        return;
    }
    console.log(`server is running at port ${PORT}`);
};

initialize();
